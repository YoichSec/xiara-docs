****************************
Deployment Considerations
****************************

Architecture
----------------------------

Xiara is intended for use as the primary interface for alert monitoring and initial incident response. 
It can be deployed alongside a SIEM or without one.
Xiara currently ingests alert data via UDP syslog, HTTP Post, or Graylog GELF UDP messages.

Xiara uses MongoDB for data storage. 


.. note::  In the future, Xiara Enterprise will be available as a cloud-hosted SAAS platform.