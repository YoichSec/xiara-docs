
****************************
Sensor Configuration
****************************

Planning Your Security Data
----------------------------
Xiara is designed to provide an efficient operational interface in lieu of the traditional log tables that analysts currently use. In doing so, we eschew the collect-everything, sort-later approach of data collection often employed in Security Operations Centers. The goal for sending security data to Xiara should be to minimize the amount of extraneous data and start with reliably actionable alerts. Survey the messaging protocols and filtering capabilities supported by your security sensors. You will create the necessary listeners in Xiara (Data Sources) and configure your sensors to send data to Xiara. Alternatively, you can use a SIEM, log management system, or similar aggregator of security data as a pass-through or pre-processor to Xiara rather than sending sensor messages directly. This could simplify your processing configuration in Xiara as well as your sensor configuration and reduce the amount of unnecessary data sent to Xiara. If you already have security sensors sending events to such a tool, the simplest way to configure and manage the data sent to Xiara would be to create a forwarder (typically syslog) on the single tool rather than adding a configuration to each of your security sensors. 
