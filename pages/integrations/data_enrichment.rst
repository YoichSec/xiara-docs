****************************
Data Enrichments
****************************

Data Enrichments integrate data from external sources to supplement the data in Xiara for the purposes of incident response and threat hunting. 

Currently, AbuseIPDB integration is available and enabled by default for IP address fields in the alert details.

.. note::  More default and user-configured Data Enrichments will soon be available. Users will be able to select Data Enrichments per Security Layer.