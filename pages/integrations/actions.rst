****************************
Actions
****************************

Actions provide automatic and manually triggered responses to security data received by Xiara.

.. note::  Default and user-created Actions will be available soon. Users will able to configure automated and manually triggered Actions per level in each Security Layer.