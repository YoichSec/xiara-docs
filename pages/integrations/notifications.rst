****************************
Notifications
****************************

Notifications automatically alert specified users in response to security data received by Xiara.

.. note::  Default and user-created Notifications will be available soon. Users will able to configure automated and manually triggered Notifications per level in each Security Layer.