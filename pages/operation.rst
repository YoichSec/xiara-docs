****************************
Operation
****************************

The following sections provide basic workflow and guidance for the operation of Xiara.

.. toctree::
   :maxdepth: 2
   
   operations/asset_topology.rst
   operations/system_management.rst