****************************
Integrations
****************************

Integrations automate processes in incident response and threat hunting and enable Xiara to exchange data with third-party services.

.. toctree::
   :maxdepth: 2
   
   Integrations/data_enrichment.rst
   Integrations/actions.rst
   Integrations/notifications.rst