****************************
Installation
****************************

.. note::  This section covers manual installation of Xiara and setup for a development environment. More deployment options will be available in the future.


Prerequisites
----------------------------
Xiara requires the following prerequisites:

* Node.js 8.11 or later
* MongoDB 3.6 or later

Xiara runs on Windows, Linux, and Mac.

Cloning the Repo
----------------------------
Clone the Xiara code from the Github repo::

 ~$ git clone https://gitlab.com/xiara/

Starting the MongoDB database
----------------------------
Start the MongoDB database with the following replica set configuration::

 ~$ mongod --dbpath <path-to-database-directory> --port 27017 --replSet "rs0" --bind_ip localhost

.. warning::  MongoDB is insecure in its default configuration. Production environments should require proper database authentication, which will be supported in a future update.

 
Starting the Xiara Server
----------------------------
Node modules must be installed prior to running the Xiara server. Run the following command in the installation directory::

 ~$ npm install

To start the Xiara server, run the following command in the installation directory::

 ~$ npm run dev-server

Starting the Xiara Client
----------------------------

To start the Xiara client, run the following command in the installation directory::

 ~$ npm run dev-client

Running this command should automatically start a browser session to Xiara.
By default, Xiara is accessed at http://localhost:3000.

