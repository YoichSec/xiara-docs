****************************
Asset Topology
****************************

The Asset Topology is Xiara's main operational interface for security monitoring, incident response, and threat hunting. The Asset Topology conveys at a glace the security state of your organization mapped onto an interactive topology of assets.

.. image:: ../../images/operations/asset_topology/assettopology.PNG
   :align: center

.. note::  The Asset Topology is optimized for readability on a 1080p or higher resolution display.

Overview
----------------------------

On initial login, the Asset Topology shows the top three asset layers of the organization. The Asset Topology is comprised of the following elements:

**Field of View** - The canvas (background) of the Asset Topology. On initial login, the Field of View represents the top asset layer, or the entire organization. 

**Parent Assets** - Large grey circles that may contain Child Assets. On initial login, each Parent Asset represents an asset in the second asset layer.

**Child Assets** - Smaller white circles contained within a Parent Asset. Security states are displayed around Child Assets.

Each subsequent drill-down from the initial view moves the Field of View, Parent Assets, and Child Assets down an asset layer.

The Asset Toplogy supports zooming in and out using the scroll-wheel and panning by clicking and dragging.  Parent assets may be moved and reorganized within the field of view, and child assets may be moved within their parents by clicking and dragging. This allows you to configure your Field of View and assets to mimic the network architecture or relative locations of your assets as you deem appropriate.

.. note::  Any movements made are not currently saved. In a near-future update, customized views will be saved per user.

Drilling Down and Navigation
----------------------------

Drill down into an asset of interest by double-clicking a child asset. Upon drilling down, the Parent Asset becomes the new Field of View, Child Assets become Parent Assets, and the Child Assets of those Parent Assets are newly visible. 

.. note::  In a near-future update, drilling down will center the Field of View around the asset that was double-clicked.

The Asset Layer Navigation pane is toggled off by default. Click the toggle button to the right of the currently logged-in user in the upper-right corner of the Xiara UI to view the navigation pane. The current Field of View is highlighted blue. After drilling down, you may move back up the asset layers by clicking on the desired Field of View in the navigation pane. 

Managing Alerts
----------------------------

Changes to security states appear on the Asset Topology in real-time without the need for polling or refreshing. Each security state greater than the base level 0 (indicated by the color green) represents one or more alerts which triggered a state change as specified in the Security Layers configuration. To view the alerts associated with a particular security state, hover over one of the colored rings surrounding a Child Asset. Its color should change slightly to indicate which security state you are currently hovering over. Click on the security state to bring up the alerts for the associated asset and Security Layer. 

The Acknowledge button for each of the alerts shown will remove the alert from the associated asset. Once all alerts are cleared for the asset and security layer, the security state is reet back to its original base level. To view the details for a particular alert, click on the alert name. The alert details view will display the associated event fields, along with any Data Enrichment, Actions, and Notifications configured. You may also add free-form notes to an alert. 