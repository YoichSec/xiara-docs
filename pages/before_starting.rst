***************
Before Starting
***************

Use this guide to plan your deployment and use of Xiara.

.. toctree::
   :maxdepth: 2
   
   before_starting/deployment_considerations.rst
   before_starting/sensor_configuration.rst