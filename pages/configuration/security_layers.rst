****************************
Security Layers
****************************

Security Layers specify how different aspects of security are monitored in Xiara.

.. image:: ../../images/configuration/security_layers/securitylayers.PNG
   :align: center

To configure a Security Layer, navigate to the Security Layers page. Click the New Layer button and provide a name and description (optional) for the new Security Layer in the first tab. Then, select the Data Source Types that will send messages to Xiara relevant to the new Security Layer. 

.. note::  A future update will allow Data Enrichments to be configured per Security Layer on the first tab.

Next, click the Levels and State Triggers tab. Here, you will define the criteria for security state changes beyond the base level 0 (green) for the new Security Layer. For each level, add the filters required and select "Any" or "All" to speify the matching criteria for the filters. 

.. note::  A future update will allow Actions and Notifications to be configured per level.

Once the filter conditions are complete for all levels, click Submit. 

.. warning::  The new Security Layer is not yet saved at this point. You must click save in the Security Layers dialogue box once satisfied with the layout of the Security Layers.

Security Layers can be reordered by dragging and dropping on the Security Layers dialog box. A visual representation of how they will appear on the Asset Topology is displayed in a diagram to the right of the dialogue box. Once you are satisfied, click save on the dialogue box to save the current configuration of Security Layers.