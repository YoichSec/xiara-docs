****************************
Data Sources
****************************

Data Sources specify how incoming messages are processed by Xiara. The configuration of Data Sources should proceed in the following order:

1. Create Data Fields for the new Data Source or ensure that they already exist.
2. Create a new Data Source Type for the new Data Source or ensure that an appropriate one already exists.
3. Create the new Data Source using a new or existing Data Connection (listening protocol and port).

Configuring Data Fields
----------------------------

Data Fields in Xiara may be shared across all Data Source Types and Data Sources. They are added manually, with the exception of several default fields. By design, Data Fields in Xiara avoid a common problem in many log management systems and SIEMs where the number of data fields grows quickly and many of the data fields have the same meaning but cannot be compared amongst each other or correlated because they have different names. In Xiara, incoming messages from various Data Sources are translated into Data Fields shared across the entire system. The goal is not to keep every bit of information received from the Data Sources, but to keep only the required information in only as many Data Fields as required. 

To create new Data Fields, navigate to the Data Fields page under the Data Sources menu item. Click New Data Field and enter a name, description (optional), and data type for the new Data Field. Then click save.

Configuring Data Source Types
----------------------------

Data Source Types define sets of Data Fields that are shared between a type of Data Source. For example, a network IDS may have one set of Data Fields regardless of vendor that differs from the set of fields relevant to endpoint protection Data Sources. In this scenario, you would define one Data Source Type for network IDS and another for endpoint protection. Then, when configuring data sources, you would select the appropriate Data Source Type to ensure that the proper Data Fields are included in the data processing for that Data Source.

To create a new Data Source Type, navigate to the Data Source Types page under the Data Sources menu item. Click New Data Source Type, then add a name and description (optional) for the new Data Source Type and select as many fields as required. Then click save.

Configuring Data Sources
----------------------------

Data Connections
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Generally, a new Data Source should be created for every new security tool sending data to Xiara. In other words, every different vendor product should likely have its own Data Source and multiple instances of the same product will share the same Data Source. You may in some instances, however, wish to process different types of messages from the same product separately in multiple Data Sources. For example, a single network device may have an IDS function as well as a DLP function. If the Key Fields or relevant Asset Types (See the Asset Layers Configuration section) differ between the two types of messages, a Data Source for each will likely be necessary. On the other hand, two products that send messages in similar enough formats (two different firewalls that send CEF syslog, for example) could potentially share the same Data Source. The number of Data Sources necessary is determined by the number of different processing pipelines required to map all of your sensor data into the required Xiara Data Fields.

Data Sources
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

To configure a Data Source, navigate to the All Data Sources page under the Data Sources menu item. First, ensure that there is an appropriate data connection for receiving the messages for the Data Source. If one does not exist, create one by clicking Add New Connection. Select the type of connection, the protocol if necessary, and port. Click save to start a listener on the port and protocol specified.

Next, click the New Data Source button to be taken to the Data Source configuration page. Enter a name and description (optional) for the new Data Source. Then, select a data connection from the drop-down menu. If there already exists one or more Data Sources on the chosen connection, you must specify a priority for the new Data Source. The priority of Data Sources on a data connection specifies the order in which new messages are evaluated against Data Source filters. Xiara attempts to match incoming messages on a data connection with the list of Data Sources configured for that connection in the order in which they appear in the Data Source priority dialog. Once a matching Data Source is found, Xiara uses the configuration for that Data Source to process the security data contained in that message. To specify Data Source priority, click the Edit Data Source Priority button and drag the New Data Source to its appropriate position in the list of configured Data Sources.

Next, you will configure how incoming messages are processed into Xiara Data Fields. You may do so by selecting a sample log and selecting message fields to map to Xiara fields or by manually entering the appropriate message field names. If you choose to select a sample log, sample log data will be shown and you will be able to choose message fields from a drop-down menu. You must also select a Data Source Type for the new Data Source or create a new one, then provide message fields to map to each of the Xiara fields for the Data Source Type. Once finished, click save and any new fields or Data Source Types created will be saved along with the newly configured Data Source.