****************************
Asset Layers
****************************

Asset Layers specify the assets tracked in Xiara and how they are grouped together to provide visual scalability.

Asset Tree
----------------------------
.. image:: ../../images/configuration/asset_layers/assettree.PNG
   :align: center

The Asset Tree displays your organization's assets in a collapsible/expandable tree diagram and allows you to add, delete, and edit new layers and assets to suit your security monitoring requirements. To initially populate the Asset Tree, a top-level node must be created. This node represents the single organization under which all of your assets will be placed. First, create a top-level Asset Layer in the Asset Layers dialogue box. Submit and save the new Asset Layer, and the Asset Tree will prompt you to create a top-level asset node. Create the top-level asset for your organization. 

Continue to add other Asset Layers under your top layer as needed. You may reorganize the asset schema by dragging and dropping layers. Once you have the appropriate layers specified, save the Asset Schema. Next, add the expected assets per Asset Layer in the Asset Tree. You may add to the tree and reorganize it at any point. The purpose of adding layers is to provide visual scalability to the Asset Topology. It is best to aim for less than 10 child assets under each parent for readability purposes. If the number of child assets in a parent exceeds this limit, child assets will be summarized to maintain readability. 

Asset Types
----------------------------

.. image:: ../../images/configuration/asset_layers/assettypes.PNG
   :align: center

Xiara allows you to monitor the security states of many different types of assets side-by-side. Common types of assets may include hosts, servers, users, applications, accounts, etc. Because different types of assets will have different data sources, security monitoring requirements, and data processing needs, you must define the appropriate asset types for your organization. 

To define an Asset Type, click the New Asset Type button on the Asset Types page. on the Asset Type tab, enter a name an an optional description. Select the Security Layers applicable to the new Asset Type.

.. note::  A future update will allow different Security Layers to be assigned per Asset Type. Currently, all Asset Types contain All Security Layers. 

Continue onto the Data Mapping tab. In this tab, you will define what determines a unique asset for the new Asset Type. Select an applicable Data Source Type and the individual Data Sources that will send security data on the new Asset Type to Xiara. From the list of Data Fields available for the selected Data Source Type, select a single Key Field that uniquely identifies an Asset of the new Asset Type. For example, a host may be uniquely identified by its IP address, while a user account may be identified by its username. 

.. note::  A future update will allow more than one Key Field to be selected per Data Source Type. The Key Fields will then be concatenated to create a single unique identifier, for example a hostname + IP address. This may be useful in situations where individual Data Field values may overlap within an organization.

Continue creating Data Mappings for as many Data Source Types as you expect to send data to Xiara for the new Asset Type.

Asset Table
----------------------------

.. image:: ../../images/configuration/asset_layers/assettable.PNG
   :align: center

The asset table on the Assets page displays all assets in a filterable table. This includes both manually added assets and assets added automatically. The Assets page allows you to assign assets to their proper Parent Assets. To assign assets to a parent, select as many assets as necessary under a common parent and click Edit Parent Asset. Select the desired Parent Asset and save. Because assets without parents are not displayed on the Asset Topology, Xiara notifies the user when new assets are added without a parent. The notifications appear in the bell icon in the top right corner of the Xiara UI.

In some instances due to partial or improper configuration, a single asset may appear multiple times in the asset table. Typically, this happens when different data source types report on the same asset using different Key Fields. You are able to merge the two instances of the same asset by selecting the rows in which they appear and clicking the Merge button.

.. note::  Currently, you can only merge two instances of an asset if they appear with different Key Fields.
