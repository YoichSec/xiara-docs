****************************
Main Configuration
****************************

Use this guide to plan your deployment and configuration of Xiara. Unlike many solutions primarily focused on the ingestion and storage of log data, the configuration of Xiara is by design a continuous process that should be thought of as part of its operation. As your organization's assets, security tooling, and threat profile change, the proper configuration and operation of Xiara ensures that your changing security requirements are appropriately addressed. 

.. toctree::
   :maxdepth: 2
   
   configuration/data_sources.rst
   configuration/security_layers.rst
   configuration/asset_layers.rst