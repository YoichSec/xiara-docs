.. Xiara Docs documentation master file, created by
   sphinx-quickstart on Thu Feb 28 18:41:51 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Xiara Documentation
======================================

.. note::  Xiara and Xiara documentation are under active development. Features and documentation will be updated frequently.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   pages/xiara_overview
   pages/before_starting
   pages/installation
   pages/configuration
   pages/integrations
   pages/operation
   pages/support
   pages/contributing