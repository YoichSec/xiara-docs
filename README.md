# Xiara

This is the repository used to create Xiara documentation on [readthedocs](https://xiara-docs.readthedocs.io/en/latest/).

You can read more about Xiara on our [website](https://xiara.io).

## Copyright and license

Xiara is released under version 3.0 of the [GNU General Public License](https://www.gnu.org/licenses/gpl-3.0.en.html). 